---
author: "George Silva"
title: Projects
subtitle: Anything interesting down here?
comments: false
date: "2018-07-10"
type: "page"
---

This page has a list of my most current projects, related to GIS
or other things.

* https://pypi.python.org/pypi/django-chaos/
* https://pypi.python.org/pypi/django-common-models/
* https://pypi.python.org/pypi/django-file-context
* https://pypi.python.org/pypi/django-workflow-fsm
* https://pypi.python.org/pypi/django-tilestache

All of these projects are poiting to the wrong repositories. Recently
we've migrated all of our open-source structure from a private Gitlab
instance to the public Gitlab instance.

In the **[geoadmin-opensource]** Gitlab group, you'll be able to find
a lot of other interesting resources and code.

**The repositories can be found here: https://gitlab.com/geoadmin-opensource**.

Another interesting resource (pt-br only) is the [geoadmin-edu] group. In there
you'll find screencasts and full courses.

## [Django Chaos]

Django Chaos is a project that laid the ground work for [Geoadmin].

It contains most of the project and task management features you would
expect on a project management system.

Since this is a open-source package, it does not try to do everything for
you. But provides the necessary hooks for notifications, cascading alterations,
etc.

Has `DRF` integrations and endpoints available.

## [Django Common Models]

This a simple Django project that contains a bunch of models that are frequently
used in my projects.

Models such as and others:

1. CreatedByMixIn
1. DateCreatedMixIn
1. DateUpdatedMixIn

There are also some common serializers and utils that we've used over the
years at [SIGMA] and [Geoadmin].

## [Django File Context]

Django File Context creates a `File` model. It allows you to attach/detach that
file to any model over your database.

Has a series of `DRF` integrations and endpoints available.

## [Django Workflow FSM]

Django Workflow FSM allows you to create and run FSM inside your
system.

It allows for configurable at runtime FSMs and Celery integration.

That means that you can create your own FSM, just by creating a few models
and then running it using the cool `DRF` integrations.

## [Django Tilestache]

Django Tilestache is a integration between Tilestache and Django.

It offers a single model, that allows you to register any kind of
`Tilestache` layer. Also it provides two specific REST endpoints:

1. Tilestache config
1. Tilestache tile

The first endpoint will generate a Tilestache configuration suitable for
usage of any Tilestache server. The second will generate and output a tile
**from your Django server**.

They are both optional. One cool thing here is that we've created a
new Tilestache executable that once in a while reconfigures itself,
by requesting a new configuration from the Tilestache Config endpoint.

 [SIGMA]: https://sigmageosistemas.com.br
 [SIGMA Geosistemas]: https://sigmageosistemas.com.br
 [Geoadmin]: https://geoadmin.com.br
 [Django Chaos]:https://pypi.python.org/pypi/django-chaos/
 [Django Common Models]: https://pypi.python.org/pypi/django-common-models/
 [Django File Context]: https://pypi.python.org/pypi/django-file-context
 [Django Workflow FSM]: https://pypi.python.org/pypi/django-workflow-fsm
 [Django Tilestache]: https://pypi.python.org/pypi/django-tilestache
 [geoadmin-opensource]: https://gitlab.com/geoadmin-opensource
 [geoadmin-edu]: https://gitlab.com/geoadmin-edu