---
author: "George Silva"
title: About me
subtitle: Who is George?
comments: false
date: "2018-07-10"
type: "page"
---

Hello everyone! My name is George and I'm a Full Stack Dev from Brazil.

I have a B.Sc. in Geography, from `Universidade Federal de Uberlândia`
here in Brazil.

I fell in love with programming and software development around 10 years ago
(it feels more) when I discovered that I could automate map publishing in
`ArcGIS`.

From there, I learned `Python`, `C#`, `PostgreSQL` and `PostGIS`, which
make up my tool belt.

I've worked with many companies and departments here in Brazil, in areas
ranging from `environment` to `mineral exploration`, going through `defense`
and `transit and transportation`.

I was a employee for a good long time, then we (me, Gustavo and Paulo) reactivated
our company, [SIGMA Geosistemas], providing consulting services for GIS
development and GIS work, in Uberlândia - Minas Gerais. In there I was in charge
of the whole development and research part.

That means that I did a lot of work developing software for our customers,
managing our team and adopting the best industry practices, such as `TDD`,
`CI`, etc. We even created our very own SaaS, **[Geoadmin]**, which is a platform
that mixes `GIS` with `project management`.

## Present time

Today I'm open for business. That means that I'm not currently formally
employed. While I had a long relation with [SIGMA], I'm no longer involved
in the company day to day business, acting as a independent contractor.

My focus is on GIS software development (Python, Django and Django Rest Framework),
with a Single Page Application front-end (such as Vue2 and React).

I'm an open source advocate and use open-source software on a daily basis.
I have a few packages (listed just a few, feel free to check others in
Github or Gitlab) published on PYPI, such as:

* django-chaos (project management);
* django-common-models (boilerplate models);
* django-file-context (attach files to anything);
* django-workflow-fsm (runtime finite state machines);
* django-tilestache (create layers in django and have them shown in TileStache);

My skills are:

1. **Python**
1. **Django/Django REST Framework**
1. **Celery**
1. JavaScript
1. Leaflet
1. **Mapbox GL JS**
1. **PostgreSQL/PostGIS**
1. Redis
1. RabbitMQ

## What is this?

Well, this is a blog/notepad for me and my crazy ideas.

Check out the blog or the projects section.

## Profiles

* [github](https://github.com/george-silva)
* [gitlab](https://gitlab.com/george-silva)
* [GIS StackExchange](https://gis.stackexchange.com/users/5/george-silva)

 [SIGMA]: https://sigmageosistemas.com.br
 [SIGMA Geosistemas]: https://sigmageosistemas.com.br
 [Geoadmin]: https://geoadmin.com.br