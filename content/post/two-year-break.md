---
date: "2020-04-02"
title: "How about this blog?"
subtitle: "Lies we tell to ourselves when we start a blog"
author: "George Rodrigues da Cunha Silva"
tags: ["personal"]
---

So, every time I start a blog, I let it fall off a few days later. Who has the discipline?

Anyways, I started this two years ago and I got only to posts.

Why can't we usually keep our promises? Well, at that time I was looking for
work and was freelancing, so blogging would come as an opportunity to scout
clients and see if the generated traffic could generate some revenue.

It didn't. Why? Well, because I was not dedicated enough to blogging.

However, a lot of other things turned out to be just fine. I soon found a cool job
at [Loadsmart](https://loadsmart.com), where I am still to this day.

I moved to another city, Florianópolis, where I still live and love.

And now we have COVID-19. Who knew?! I never imagined that I would face such
uncertainty and troubling times.

What is going to happen in a year? Are all markets going to crash and burn as
mankind returns to the it's infancy, with sticks and stones?

Are we going to be just fine in a few months? I don't know.

Me and my team are still working remotely, so for us is a bit business as
usual. Of course, it's not as usual because the world is a bit upside down, but
we are all well and alive.

Also I rediscovered Magic: The Gathering. I am playing a lot in Arena and in paper (well I was
playing on paper).

I'll try blogging about that also.

Let's see what the next 2 year post brings?
