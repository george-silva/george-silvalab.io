---
date: "2018-07-26"
title: "New Challenges"
author: "George Rodrigues da Cunha Silva"
subtitle: "Change is good"
tags: ["personal"]
---

So, this is my first post. It's been stuck here for some time and I had a lot of things to take
of, leaving this as it were. Shame on me.

I've always liked blogging and I already had two or three somewhat successful blogs, mostly known
by the people on the GIS (Geographic Information Systems) front. This should be no different :D.

So, new news first: I have been an associate of a really cool company called [SIGMA GEOSISTEMAS] for
a little over ten years. Time flies. Me and my college buddies, Gustavo and Paulo (and Marcos, but
he left years ago) built [SIGMA] to be a one stop shop for `GIS`. So we did from Remote Sensing to
software development.

After ten good years, it's time to face new challenges and meet new people. So I said goodbye to [SIGMA
GEOSISTEMAS] (and [Geoadmin], a *Software as a Service* that puts GIS together with project management)
in May of 2018.

During this time, I've been freelancing for some really cool people in Canada, working with
what I love the most: `Python` and `GIS`! We are working on some interesting projects, that involve
multiple `GIS` data formats, social interaction between scientists and the general people.

So far it's looking good. Actually, we are just finishing this project, after three or four months of
development (could be much shorter, but I was involved with [SIGMA] before, so this not full time).

This change of pace is very much welcome. Freelancing is a bit hard, because now you're at home,
with a delivery on your hands and you need to find some balance between **overworking** and **staying
in pajamas** all day long. But it's doable.

So, I'm kicking off this blog with a post about change. Change is good. It forces you to reevaluate
a lot of things, your work priorities, where do you want to go and how do you want to get there.

This stuff is important. I'm now `flying solo`.

I want to use this post as an opportunity to also say a big thanks for all those wonderful years.
Thank you Gustavo and Paulo Vitor. You are wonderful friends and associates and I wish a lot
of success for [SIGMA] and [Geoadmin].

*By the way, if you need GIS consulting services, from remote sensing to software development,
talk to these guys. They know what they're doing!*

 [SIGMA]: https://sigmageosistemas.com.br
 [SIGMA GEOSISTEMAS]: https://sigmageosistemas.com.br
 [Geoadmin]: https://geoadmin.com.br